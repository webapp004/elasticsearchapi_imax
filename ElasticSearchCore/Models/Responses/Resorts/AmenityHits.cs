﻿
namespace ElasticSearch.Models.Responses.Resorts
{
    public class ResortHits
    {
        public ResortHit[] hits { get; set; }
    }
}
