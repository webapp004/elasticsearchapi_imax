﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Calendars
{
    public class CalendarSource
    {
        public string ConfirmationMethod { get; set; }
        public DateTime Date { get; set; }
        public decimal Rate { get; set; }
        public long InventoryID { get; set; }
    }
}
