﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Calendars
{
    public class CalendarHits
    {
        public CalendarHit[] hits { get; set; }
    }
}
