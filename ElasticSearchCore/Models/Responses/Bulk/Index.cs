﻿using ElasticSearch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Bulk
{
    public class Index
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _version { get; set; }
        public string _result { get; set; }
        public Shard shards { get; set; }
        public string created { get; set; }
        public string status { get; set; }
    }
}
