﻿using ElasticSearch.Models.Responses.WeeklyRates;

namespace ElasticSearch.Models.Responses
{
    public class ElasticWeeklyRateSearchResult
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public WeeklyRateHits hits { get; set; }
    }
}
