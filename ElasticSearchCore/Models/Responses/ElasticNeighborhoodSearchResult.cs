﻿using ElasticSearch.Models.Responses.Neighborhoods;

namespace ElasticSearch.Models.Responses
{
    public class ElasticNeighborhoodSearchResult
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public NeighborhoodHits hits { get; set; }
    }
}
