﻿
namespace ElasticSearch.Models.Responses.Neighborhoods
{
    public class NeighborhoodSource
    {
        public long InventoryID { get; set; }
        public string Neighborhood { get; set; }
    }
}
