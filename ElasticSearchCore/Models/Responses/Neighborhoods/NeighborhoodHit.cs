﻿
namespace ElasticSearch.Models.Responses.Neighborhoods
{
    public class NeighborhoodHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public NeighborhoodSource _source { get; set; }
    }
}
