﻿using ElasticSearch.Models.Responses.Properties;

namespace ElasticSearch.Models.Responses
{
    public class ElasticPropertySearchResults
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public PropertyHits hits { get; set; }
    }
}
