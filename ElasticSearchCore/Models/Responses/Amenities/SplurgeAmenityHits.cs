﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Amenities
{
    public class SplurgeAmenityHits
    {
        public SplurgeAmenityHit[] hits { get; set; }
    }
}
