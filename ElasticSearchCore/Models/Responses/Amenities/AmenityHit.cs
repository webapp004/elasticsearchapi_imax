﻿
namespace ElasticSearch.Models.Responses.Amenities
{
    public class AmenityHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public AmenitySource _source { get; set; }
    }
}
