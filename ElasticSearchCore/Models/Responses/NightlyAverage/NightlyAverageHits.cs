﻿
namespace ElasticSearch.Models.Responses.NightlyAverage
{
    public class NightlyAverageHits
    {
        public NightlyAverageHit[] hits { get; set; }
    }
}
