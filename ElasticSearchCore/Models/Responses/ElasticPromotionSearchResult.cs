﻿using ElasticSearch.Models.Responses.Promotions;

namespace ElasticSearch.Models.Responses
{
    public class ElasticPromotionSearchResult
    {
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public PromotionHits hits { get; set; }
    }
}
