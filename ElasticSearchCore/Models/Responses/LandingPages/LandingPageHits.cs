﻿
namespace ElasticSearch.Models.Responses.LandingPages
{
    public class LandingPageHits
    {
        public LandingPageHit[] hits { get; set; }
    }
}
