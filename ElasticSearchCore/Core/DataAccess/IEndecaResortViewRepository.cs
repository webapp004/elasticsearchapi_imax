﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaResortViewRepository
    {
        List<EndecaResortView> GetAllResorts();
        List<EndecaResortView> GetAllResortsWithProperties();
        List<EndecaResortView> GetResortsByInventoryID(long inventoryID);
        List<long> GetInventoryIDsByResortName(List<string> resortNames);
    }
}
