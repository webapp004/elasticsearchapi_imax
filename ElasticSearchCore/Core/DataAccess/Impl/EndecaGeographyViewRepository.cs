﻿using System.Collections.Generic;
using System.Linq;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaGeographyViewRepository: IEndecaGeographyViewRepository
    {
        private Connection _connection;

        public EndecaGeographyViewRepository()
        {
            _connection = new Connection();
        }

        public List<EndecaGeographyView> GetAllGeography()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaGeographyViews
                        select x).ToList();
            }
        }

        public List<EndecaGeographyView> GetAllGeographyThatHaveProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.EndecaGeographyViews
                        on x.InventoryID equals y.InventoryID
                        select y).ToList();
            }
        }

        public List<EndecaGeographyView> GetGeographyByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaGeographyViews
                        where x.InventoryID == inventoryID
                        select x).ToList();
            }
        }

        public List<long> GetInventoryIDsByCountry(List<string> countries)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> inventoryIDs = new List<long>();

                foreach(string country in countries)
                {
                    inventoryIDs.AddRange((from x in context.EndecaGeographyViews
                                        where x.Country.ToLower() == country.ToLower()
                                        select x.InventoryID).ToList());
                }

                return inventoryIDs;
            }
        }

        public List<long> GetInventoryIDsByDestination(List<string> destinations)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> inventoryIDs = new List<long>();

                foreach (string destination in destinations)
                {
                    inventoryIDs.AddRange((from x in context.EndecaGeographyViews
                                           where x.Destination.ToLower() == destination.ToLower()
                                           select x.InventoryID).ToList());
                }

                return inventoryIDs;
            }
        }

        public List<long> GetInventoryIDsByDestinationCategoryName(List<string> destinationCategoryNames)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> inventoryIDs = new List<long>();

                foreach (string destinationCatName in destinationCategoryNames)
                {
                    inventoryIDs.AddRange((from x in context.EndecaGeographyViews
                                           where x.DestinationCategoryName.ToLower() == destinationCatName.ToLower()
                                           select x.InventoryID).ToList());
                }

                return inventoryIDs;
            }
        }
    }
}
