﻿using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses.Calendars;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Gets the rates and availability for a given unit/property.
    /// </summary>
    public class GetRatesAndAvailabilityController : ApiController
    {
        /// <summary>
        /// Retrieves rates and availability for an individual unit/property
        /// </summary>
        /// <param name="calendarRequest">The calendar request object</param>
        /// <returns></returns>
        public List<CalendarSource> Post([FromBody]CalendarRequest calendarRequest)
        {
            CalendarQuery calendarQuery = new CalendarQuery();
            List<CalendarHit> calHits = calendarQuery.GetRatesAndAvailability(calendarRequest.InventoryID);

            return (from x in calHits
                    select x._source).ToList();
        }
    }
}
