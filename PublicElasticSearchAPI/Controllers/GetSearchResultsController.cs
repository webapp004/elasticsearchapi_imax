﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearchCore.API.ElasticQueries;
using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Gets inventory search results, based upon the parameters passed in.
    /// </summary>
    public class GetSearchResultsController : ApiController
    {
        /// <summary>
        /// Returns inventory search results, based upon the request provided
        /// </summary>
        /// <param name="searchRequest">The search request object</param>
        /// <returns></returns>
        public SearchResults Post([FromBody]SearchRequest searchRequest)
        {
            Helpers helper = new Helpers();

            // Paging parameters, passed in from user
            int pageSize = searchRequest.PageSize;
            int pageNumber = searchRequest.PageNumber;

            // Get the base query results
            Tuple<List<PropertyHit>, List<PropertyHit>> baseQueryProperties = GetBaseQueryResults(searchRequest);

            //set up the sorted properties
            List<PropertyHit> sortedProperties = baseQueryProperties.Item1;
            List<PropertyHit> sortedFeatures = baseQueryProperties.Item2;

            // Filter properties by amenities, if any
            if(searchRequest.Amenities != null)
            {
                if(searchRequest.Amenities.Count() > 0)
                {
                    sortedProperties = helper.FilterByAmenities(searchRequest.Amenities, sortedProperties);
                }
            }

            //Add a numerical rate to the base query results, for sorting
            helper.FillInRatesAndMerchOrder(sortedProperties);

            //Sort properties
            //0 = default, 1 = Pricing Low to High, 2 = Pricing High to Low, 3 = Sleeps Low to High, and 4 = Sleeps High to Low
            //5 = Bedrooms High to Low, and 6 = Bedrooms Low to High
            switch (searchRequest.Sorting)
            {
                case 0: //0 = default
                    sortedProperties = sortedProperties.OrderBy(x => x.merchandiceOrder).ToList();
                    break;
                case 1: //1 = Pricing Low to High
                    sortedProperties = sortedProperties.OrderBy(x => x.rate).ToList();
                    break;
                case 2: //2 = Pricing High to Low
                    sortedProperties = sortedProperties.OrderByDescending(x => x.rate).ToList();
                    break;
                case 3: //3 = Sleeps Low to High
                    sortedProperties = sortedProperties.OrderBy(x => x._source.Sleeps).ToList();
                    break;
                case 4: //4 = Sleeps High to Low
                    sortedProperties = sortedProperties.OrderByDescending(x => x._source.Sleeps).ToList();
                    break;
                case 5: //5 = Bedrooms High to Low
                    sortedProperties = sortedProperties.OrderByDescending(x => x._source.Bedroom).ToList();
                    break;
                case 6: //6 = Bedrooms Low to High
                    sortedProperties = sortedProperties.OrderBy(x => x._source.Bedroom).ToList();
                    break;
                default: //0 = default
                    sortedProperties = sortedProperties.OrderBy(x => x.merchandiceOrder).ToList();
                    break;
            }

            // Get current refinements (Amenities, Destinations, Sleeps, etc.), using the sorted properties
            List<Dictionary<string, int>> searchRefinements = helper.GetSearchRefinements(sortedProperties);
            
            // Get the correct page for the results
            List<PropertyHit> subset = new List<PropertyHit>();
            int recordOffset = pageSize * pageNumber;

            if (sortedProperties.Count > (recordOffset + pageSize))
            {
                subset = sortedProperties.GetRange(recordOffset != 0 ? recordOffset - 1 : 0, pageSize);
            }
            else if (recordOffset == 0 && sortedProperties.Count < pageSize)
            {
                subset = sortedProperties;
            }
            else if ((recordOffset + pageSize) - sortedProperties.Count < pageSize)
            {
                subset = sortedProperties.GetRange(recordOffset - 1, sortedProperties.Count - recordOffset);
            }

            SearchResults searchResults = new SearchResults()
            {
                Inventories = helper.FillPropertyInfos((from x in subset select x._source.InventoryID).ToList()),
                FeaturedInventories = helper.FillPropertyInfos((from x in sortedFeatures select x._source.InventoryID).ToList()),
                Amenities = searchRefinements[0],
                Destinations = searchRefinements[1],
                Cities = searchRefinements[2],
                States = searchRefinements[3],
                Countries = searchRefinements[4],
                Sleeps = searchRefinements[5],
            };

            return searchResults;
        }

        private Tuple<List<PropertyHit>, List<PropertyHit>> GetBaseQueryResults(SearchRequest searchRequest)
        {
            PropertyQuery propertyQuery = new PropertyQuery();
            List<PropertyHit> propertySources = new List<PropertyHit>();
            List<PropertyHit> featuredPropertySources = new List<PropertyHit>();
            string status = "active";

            if (searchRequest.Country.Count() != 0)
            {
                foreach (string c in searchRequest.Country)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesByCountry(c);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            #region destinations
            //if (destinations.Count > 0)
            //{
            //    foreach (string d in destinations)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyDestination(d);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}
            #endregion

            #region destinationType
            //if (destinationTypeCategories.Count > 0)
            //{
            //    foreach (string d in destinationTypeCategories)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyDestinationCategoryName(d);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}
            #endregion

            #region resorts
            //if (resorts.Count > 0)
            //{
            //    foreach (string r in resorts)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyResort(r);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}
            #endregion

            if (searchRequest.City.Count > 0)
            {
                foreach (string c in searchRequest.City)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyCity(c);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (searchRequest.State.Count > 0)
            {
                foreach (string s in searchRequest.State)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyState(s);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            #region neighborhoods
            //if (neighborhoods.Count > 0)
            //{
            //    foreach (string n in neighborhoods)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyNeighborhood(n);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}
            #endregion

            if (searchRequest.Sleeps.Count > 0)
            {
                foreach (int b in searchRequest.Sleeps)
                {
                    ElasticPropertySearchResults propertyResults = null;

                    if (searchRequest.IsLuxury == null)
                    {
                        propertyResults = propertyQuery.GetLuxuryPropertiesbyBed(b.ToString());
                        propertyResults = propertyQuery.GetPropertiesbyBed(b.ToString());
                    }
                    else if ((bool)searchRequest.IsLuxury)
                        propertyResults = propertyQuery.GetLuxuryPropertiesbyBed(b.ToString());
                    else
                        propertyResults = propertyQuery.GetPropertiesbyBed(b.ToString());

                    propertySources.AddRange(propertyResults.hits.hits);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (searchRequest.IsLuxury != null)
            {
                if ((bool)searchRequest.IsLuxury)
                {
                    propertySources = (from x in propertySources
                                       where x._source.IsLuxury == true
                                       select x).ToList();
                }
            }

            if (status != string.Empty)
            {
                propertySources = (from x in propertySources
                                   where x._source.Status.ToLower().Equals(status.ToLower())
                                   select x).ToList();
            }

            featuredPropertySources = (from x in propertySources
                                       where x._source.IsFeaturedProperty == true
                                       select x).ToList();

            return new Tuple<List<PropertyHit>, List<PropertyHit>>(propertySources, featuredPropertySources);
        }
    }
}
