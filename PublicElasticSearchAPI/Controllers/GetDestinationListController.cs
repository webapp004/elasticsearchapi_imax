﻿using ElasticSearch.Models.Responses.Geography;
using ElasticSearchCore.API.ElasticQueries;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Get a list of destinations
    /// </summary>
    public class GetDestinationListController : ApiController
    {
        /// <summary>
        ///  Gets a list of destinations, to be used when querying the API for properties
        /// </summary>
        /// <param name="auth">Realvoice authentication object</param>
        /// <returns></returns>
        public IEnumerable<string> Post([FromBody]DestinationRequest destReq)
        {
            GeographyQuery geographyQuery = new GeographyQuery();
            List<GeographyHit> geoHits = geographyQuery.GetAllGeographies();

            List<string> destinations = (from x in geoHits
                                         select x._source.Destination).ToList();

            return destinations.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();
        }
    }
}
