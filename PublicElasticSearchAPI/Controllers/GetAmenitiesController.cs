﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Amenities;
using ElasticSearchCore.API.ElasticQueries;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Gets the amenities for a given inventory ID
    /// </summary>
    public class GetAmenitiesController : ApiController
    {
        /// <summary>
        /// Gets a full list of amenities for a given inventory ID.
        /// </summary>
        /// <param name="amenityRequest">The request for amenities</param>
        public IEnumerable<string> Post([FromBody]AmenityRequest amenityRequest)
        {
            AmenityQuery amenityQuery = new AmenityQuery();
            ElasticAmenitySearchResult result = amenityQuery.GetAmenitiesByInventoryID((long)amenityRequest.InventoryID);
            List<string> amenities = new List<string>();

            foreach (AmenityHit hit in result.hits.hits)
            {
                amenities.Add(hit._source.AttribName);
            }

            return amenities;
        }
    }
}
