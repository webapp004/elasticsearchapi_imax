﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearch.Models.Responses.RentalUnitContent;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses;
using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Returns information about a unit/property
    /// </summary>
    public class GetPropertyInfoController : ApiController
    {
        // POST: api/GetPropertyInfo
        /// <summary>
        /// Returns information about a given unit/property
        /// </summary>
        /// <param name="propertyInfoRequest">The property info request</param>
        public PropertyInfo Post([FromBody]PropertyInfoRequest propertyInfoRequest)
        {
            Helpers helpers = new Helpers();
            List<PropertyInfo> properties = helpers.FillPropertyInfos(new List<long>() { propertyInfoRequest.InventoryID });

            if(properties.Count == 1)
            {
                return properties[0];
            }

            return new PropertyInfo();
        }
    }
}
