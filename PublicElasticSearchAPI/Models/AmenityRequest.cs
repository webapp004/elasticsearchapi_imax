﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// Gets amenities for a given property. Used for display purposes.
    /// </summary>
    public class AmenityRequest
    {
        /// <summary>
        /// The RealVoice inventory ID numbers you want to get amenities for
        /// </summary>
        public long InventoryID { get; set; }
        /// <summary>
        /// Authentication for the Realvoice API
        /// </summary>
        [Required]
        public Authentication Authentication { get; set; }
    }
}