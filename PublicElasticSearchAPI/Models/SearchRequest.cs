﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// Contains parameters that are ORed together, except Amenities(optional) and SearchString(optional), which are ANDed with everything else (if included in a search).
    /// </summary>
    public class SearchRequest
    {
        /// <summary>
        /// The country in which properties are desired.
        /// </summary>
        public List<string> Country { get; set; }
        /// <summary>
        /// The destination in which properties are desired.
        /// </summary>
        public List<string> Destination { get; set; }
        /// <summary>
        /// The city in which properties are desired.
        /// </summary>
        public List<string> City { get; set; }
        /// <summary>
        /// The state in which properties are desired.
        /// </summary>
        public List<string> State { get; set; }
        /// <summary>
        /// Search for luxury properties. Null = all results, true = luxury, and false = non-luxury results.
        /// </summary>
        public bool? IsLuxury { get; set; }
        /// <summary>
        /// The number of people a property/unit can sleep.
        /// </summary>
        public List<int> Sleeps { get; set; }
        /// <summary>
        /// The lodging type being queried. Can be Apartment, BedBreakfast, CampingGround, Condo, Home, Hotel_AllInclusive, Hotel_Hotel, Hotel_Inn, Hotel_Lodge, Lodge, Yacht
        /// </summary>
        public List<string> LodgingType { get; set; }
        /// <summary>
        /// A list of required amenities, by name.
        /// </summary>
        public List<string> Amenities { get; set; }
        /*/// <summary>
        /// Text to search by
        /// </summary>
        public string SearchString { get; set; }*/
        /// <summary>
        /// 0 = default, 1 = Pricing Low to High, 2 = Pricing High to Low, 3 = Sleeps Low to High, 4 = Sleeps High to Low, 5 = Bedrooms High to Low, and 6 = Bedrooms Low to High
        /// </summary>
        [Required]
        [Range(0,6)]
        public int Sorting { get; set; }
        /// <summary>
        /// How many results to be returned.
        /// </summary>
        [Required]
        public int PageSize { get; set; }
        /// <summary>
        /// The page of results to return. Starts at page 0.
        /// </summary>
        [Required]
        public int PageNumber { get; set; }
        /// <summary>
        /// Authentication for the Realvoice API
        /// </summary>
        [Required]
        public Authentication Authentication { get; set; }
    }
}