﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// Gets a full list of amenities, used for searching for properties.
    /// </summary>
    public class FullAmenityRequest
    {
        /// <summary>
        /// Authentication for the Realvoice API
        /// </summary>
        [Required]
        public Authentication Authentication { get; set; }
    }
}