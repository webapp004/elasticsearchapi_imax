﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IEndecaQuerySpecification
    {
        int MaxReturn { get; set; }

        int RecordOffset { get; set; }

        ReadOnlyCollection<SortKey> SortKeys { get; }
        void AddSortKey(SortKey sortKey);
        void InsertSortKey(int index, SortKey sortKey);
        void ClearSort();
        string GetSortString();

        EndecaRecordFilterBase RecordFilter { get; set; }

        TravelDates TravelDates { get; set; }

        string SearchTerm { get; }
        string SearchKey { get; }
        void SetSearch(string searchTerm, string searchKey);
        void ClearSearch();
        bool HasSearch { get; }

        ReadOnlyCollection<EndecaDimension> ExposedRefinementDimensions { get; }
        void AddExposedRefinementDimension(EndecaDimension dimension);
        void AddExposedRefinementDimensions(IEnumerable<EndecaDimension> dimensions);
        void AddExposedRefinementDimensions(params EndecaDimension[] dimensions);

        ReadOnlyCollection<EndecaDimensionValue> NavigationDimensionValues { get; }
        void AddNavigationDimensionValue(EndecaDimensionValue dimensionValue);

        ReadOnlyCollection<long> NavigationDimensionValueIds { get; }
        void AddNavigationDimensionValueId(long dimensionValueId);

        void ConfigureFromSearchResultsUrl(SearchResultsUrl searchResultsUrl);
    }
}
