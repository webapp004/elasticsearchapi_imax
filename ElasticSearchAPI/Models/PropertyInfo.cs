﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VacationRoost.Domain.RentalUnits.Amenities;

namespace ElasticSearchAPI.Models
{
    public class PropertyInfo : IBasicRentalUnit
    {
        public string spec { get; set; }
        public List<DimensionRefinement> dimensions = new List<DimensionRefinement>();

        long IBasicRentalUnit.Id
        {
            get { return RentalUnitId; }
        }
        public int RentalUnitId { get; set; }
        public long InventoryId { get; set; }

        public string PromoText { get; set; }

        public string Description { get; set; }
        public int ImageCount { get; set; }
        public int Sleeps { get; set; }
        public string VirtualTourUrl { get; set; }
        public string ExpertReview { get; set; }
        public string EmailBooking { get; set; }
        public string PropertyDisclaimer { get; set; }
        public string Policies { get; set; }
        public bool IsLuxury { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public string Island { get; set; }
        public string Status { get; set; }

        public decimal? FromRate { get; set; }

        public decimal? SpecialFromRate { get; set; }

        public DateTime? FromRateStartDate { get; set; }
        public DateTime? FromRateEndDate { get; set; }
        public string CurrencyType { get; set; }

        public string SpecialDescription { get; set; }
        public DateTime? SpecialStartDate { get; set; }
        public DateTime? SpecialEndDate { get; set; }
        public DateTime? SpecialBookByDate { get; set; }
        public string SpecialExclusions { get; set; }
        public string SpecialTermsAndConditions { get; set; }
        public GatewayProviderType GatewayProviderType { get; set; }
        public string GatewayPropertyTypeCode { get; set; }
        public float? PropertyRating { get; set; }

    }
}