﻿using ElasticSearchAPI.Models.Interfaces;

namespace ElasticSearchAPI.Models.Responses
{
    public class ElasticSearchRentalUnit
    {
        public long Id { get; set; }
        public long InventoryId { get; set; }
        public bool RatesRequireVerification { get; set; }
        public bool IsPrivateCalendar { get; set; }
        public IRoomType RoomType { get; set; }
        public string[] TopTwoTags { get; set; }
        public string FullName { get; set; }
    }
}