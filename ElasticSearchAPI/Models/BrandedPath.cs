﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class BrandedPath : IBrandedPath
    {
        public BrandedPath(string virtualPath)
        {
            if (string.IsNullOrEmpty(virtualPath))
            {
                throw new ArgumentException("Argument cannot be null or empty.", "virtualPath");
            }

            if (!virtualPath.StartsWith("/") || !IsBrandedPath(virtualPath))
            {
                throw new ArgumentException("Argument must be a branded virtial path. For example: \"/vacationroost.com/home.aspx\"", "virtualPath");
            }

            InternalPath = virtualPath;
        }

        public BrandedPath(Uri url)
        {
            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (!IsBrandedPath(url.AbsolutePath))
            {
                throw new ArgumentException("Argument must be a branded URL. For example: \"http://www.vacationroost.com/vacationroost.com/home.aspx\"", "url");
            }

            InternalPath = url.AbsolutePath;
        }

        public BrandedPath(string baseDomain, string rootRelativePath)
        {
            if (string.IsNullOrEmpty(baseDomain))
            {
                throw new ArgumentException("Argument cannot be null or empty.", "baseDomain");
            }

            if (string.IsNullOrEmpty(rootRelativePath))
            {
                throw new ArgumentException("Argument cannot be null or empty.", "rootRelativePath");
            }

            if (!baseDomain.EndsWith("/") && !rootRelativePath.StartsWith("/"))
            {
                baseDomain += "/";
            }
            int qLocation = rootRelativePath.IndexOf('?');
            if (qLocation > 0)
            {
                rootRelativePath = rootRelativePath.Substring(0, qLocation);
            }

            InternalPath = "/" + baseDomain + rootRelativePath;
        }

        private string _internalPath;
        private string InternalPath
        {
            get
            {
                return _internalPath;
            }
            set
            {
                _internalPath = value.ToLower();
                PathParts = GetPathParts(value);
            }
        }

        public ReadOnlyCollection<string> PathParts { get; private set; }

        private static ReadOnlyCollection<string> GetPathParts(string path)
        {
            return new ReadOnlyCollection<string>(path.Trim('/').ToLower().Split('/'));
        }

        public static bool IsBrandedPath(string path)
        {
            ReadOnlyCollection<string> pathParts = GetPathParts(path);

            if (pathParts.Count == 0)
            {
                return false;
            }

            if (pathParts[0].EndsWith(".net") || pathParts[0].EndsWith(".com") || pathParts[0].EndsWith(".org") || pathParts[0].EndsWith(".info") || pathParts[0].EndsWith(".mx") || pathParts[0].EndsWith(".co"))
            {
                return true;
            }

            return false;
        }

        public string BaseDomain
        {
            get
            {
                return PathParts[0];
            }
        }

        public string PathWithoutDomain
        {
            get
            {
                var remainingParts = new List<string>();

                for (int i = 1; i < PathParts.Count; i++)
                {
                    remainingParts.Add(PathParts[i]);
                }

                return "/" + string.Join("/", remainingParts.ToArray());
            }
        }

        public string ClientPath
        {
            get
            {
                return PathWithoutDomain.Replace("/guided-search.aspx", "");
            }
        }

        public string SubHome
        {
            get
            {
                string path = PathWithoutDomain;

                //The sub home is a destination type name as any part of the URL

                var validSubHomes = new List<string>
                {
                    "ski",
                    "beach",
                    "outdoor",
                };

                foreach (string pathPart in PathParts)
                {
                    if (validSubHomes.Contains(pathPart))
                    {
                        return pathPart;
                    }
                }

                return string.Empty;
            }
        }

        public string PageKey
        {
            get
            {
                var resultParts = new List<string>();

                //The first path part is the domain so skip that
                for (int i = 1; i < PathParts.Count; i++)
                {
                    resultParts.Add(PathParts[i].ToLower().Replace(".aspx", "").Replace(".htm", ""));
                }

                return string.Join(".", resultParts.ToArray());
            }
        }

        public IBrandedPath GetPathWithoutSearch()
        {
            if (PathWithoutDomain.EndsWith("/search/guided-search.aspx"))
            {
                return new BrandedPath(BaseDomain, PathWithoutDomain.Replace("/search/guided-search.aspx", "/guided-search.aspx"));
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return Equals(InternalPath, ((BrandedPath)obj).InternalPath);
        }

        public override int GetHashCode()
        {
            return InternalPath.GetHashCode();
        }

        public override string ToString()
        {
            return BaseDomain + PathWithoutDomain;
        }
    }
}