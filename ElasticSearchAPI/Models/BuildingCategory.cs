﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class BuildingCategory : CodeValue<BuildingCategory>
    {
        public static class Codes
        {
            public const string HOTEL = "HOTEL";
            public const string BEDBREAKFAST = "BEDBREAKFAST";
            public const string CONDO = "CONDO";
            public const string APARTMENT = "APARTMENT";
            public const string HOME = "HOME";
            public const string CAMPINGGROUND = "CAMPINGGROUND";
            public const string YACHT = "YACHT";
        }

        public readonly static BuildingCategory Hotel = new BuildingCategory(Codes.HOTEL, "Hotel");
        public readonly static BuildingCategory BedBreakfast = new BuildingCategory(Codes.BEDBREAKFAST, "Bed & Breakfast");
        public readonly static BuildingCategory Condo = new BuildingCategory(Codes.CONDO, "Condo");
        public readonly static BuildingCategory Apartment = new BuildingCategory(Codes.APARTMENT, "Apartment");
        public readonly static BuildingCategory Home = new BuildingCategory(Codes.HOME, "Home");
        public readonly static BuildingCategory CampingGround = new BuildingCategory(Codes.CAMPINGGROUND, "Camping Ground");
        public readonly static BuildingCategory Yacht = new BuildingCategory(Codes.YACHT, "Yacht");

        public BuildingCategory(string code, string description)
            : base(code, description)
        {
        }
    }
}