﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class Pair<T1, T2>
    {
        public Pair(T1 first, T2 second)
        {
            _first = first;
            _second = second;
        }

        private readonly T1 _first;
        public T1 First { get { return _first; } }
        private readonly T2 _second;
        public T2 Second { get { return _second; } }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (!(obj is Pair<T1, T2>))
            {
                return false;
            }
            var pair = (Pair<T1, T2>)obj;

            return DoEqual(First, pair.First) && DoEqual(Second, pair.Second);
        }

        public override int GetHashCode()
        {
            return ObjectUtilities.CombineHashCodes(DoHashCode(First), DoHashCode(Second));
        }

        private static bool DoEqual<X>(X one, X two)
        {
            if (one != null)
            {
                return one.Equals(two);
            }
            if (two != null)
            {
                return two.Equals(one);
            }
            return true;
        }

        private static int DoHashCode<X>(X obj)
        {
            if (obj != null)
            {
                return obj.GetHashCode();
            }
            return 0;
        }
    }

    public class Pair<T> : Pair<T, T>, IEnumerable<T>
    {
        public Pair(T first, T second) : base(first, second)
        {
        }

        public IEnumerator<T> GetEnumerator()
        {
            yield return First;
            yield return Second;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public static class PairUtilities
    {
        public static IEnumerable<T> UnionDepth<T>(this IEnumerable<Pair<T>> pairs)
        {
            foreach (Pair<T> pair in pairs)
            {
                foreach (T item in pair)
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<T> UnionBreadth<T>(this IEnumerable<Pair<T>> pairs)
        {
            var seconds = new List<T>();
            foreach (Pair<T> pair in pairs)
            {
                yield return pair.First;
                seconds.Add(pair.Second);
            }
            foreach (T item in seconds)
            {
                yield return item;
            }
        }
    }
}