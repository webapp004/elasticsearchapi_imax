﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Amenities;
using ElasticSearch.Models.Responses.Geography;
using ElasticSearch.Models.Responses.Neighborhoods;
using ElasticSearch.Models.Responses.NightlyAverage;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearch.Models.Responses.RentalUnitContent;
using ElasticSearch.Models.Responses.WeeklyRates;
using ElasticSearchAPI.Models.ReturnToClient;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Core.DataAccess.Impl;
using ElasticSearchCore.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ElasticSearchAPI.Models.ControllerHelpers
{
    public class Helpers
    {
        /// <summary>
        /// Given a list of PropertyHits, get a list of refinements for these given inventoryIDs
        /// </summary>
        /// <param name="propertyHits"></param>
        /// <returns></returns>
        public List<RefinementDimensions> GetRefinements(List<PropertyHit> propertyHits)
        {
            List<Refinement> destinationRefinements = null;
            List<Refinement> cityRefinements = null;
            List<Refinement> neighborhoodRefinements = null;
            List<Refinement> bedroomRefinements = null;
            List<Refinement> sleepsRefinements = null;
            List<Refinement> amenitiesRefinements = null;
            List<Refinement> buildingCateoryRefinements = null;
            List<Refinement> buildingTypeRefinements = null;
            List<Refinement> stateRefinements = null;

            Task[] tasks = new Task[9];

            tasks[0] = Task.Run(() => { destinationRefinements = GetDestinationRefinements(propertyHits); });
            tasks[1] = Task.Run(() => { cityRefinements = GetCityRefinements(propertyHits); });
            tasks[2] = Task.Run(() => { neighborhoodRefinements = GetNeighborhoodRefinements(propertyHits); });
            tasks[3] = Task.Run(() => { bedroomRefinements = GetBedroomRefinements(propertyHits); });
            tasks[4] = Task.Run(() => { sleepsRefinements = GetSleepsRefinements(propertyHits); });
            tasks[5] = Task.Run(() => { amenitiesRefinements = GetAmenitiesRefinements(propertyHits); });
            tasks[6] = Task.Run(() => { buildingCateoryRefinements = GetBuildingCategoryRefinements(propertyHits); });
            tasks[7] = Task.Run(() => { buildingTypeRefinements = GetBuildingTypeRefinements(propertyHits); });
            tasks[8] = Task.Run(() => { stateRefinements = GetStateRefinements(propertyHits); });

            Task.WaitAll(tasks);

            RefinementDimensions orDestinations = new RefinementDimensions()
            {
                name = "OR_Destination",
                id = 162,
                refinements = destinationRefinements
            };
            RefinementDimensions orCity = new RefinementDimensions()
            {
                name = "OR_City",
                id = 164,
                refinements = cityRefinements
            };
            RefinementDimensions orNeighborhood = new RefinementDimensions()
            {
                name = "OR_Neighborhood",
                id = 270,
                refinements = neighborhoodRefinements
            };
            RefinementDimensions orBedrooms = new RefinementDimensions()
            {
                name = "OR_Bedrooms",
                id = 250,
                refinements = bedroomRefinements
            };
            RefinementDimensions orSleeps = new RefinementDimensions()
            {
                name = "OR_Sleeps",
                id = 264,
                refinements = sleepsRefinements
            };
            RefinementDimensions andAmenities = new RefinementDimensions()
            {
                name = "AND_Amenities",
                id = 171,
                refinements = amenitiesRefinements
            };
            RefinementDimensions orBuildingCategory = new RefinementDimensions()
            {
                name = "OR_BuildingCategory",
                id = 331,
                refinements = buildingCateoryRefinements
            };
            RefinementDimensions orBuildingType = new RefinementDimensions()
            {
                name = "OR_BuildingType",
                id = 337,
                refinements = buildingTypeRefinements
            };
            RefinementDimensions orState = new RefinementDimensions()
            {
                name = "OR_State",
                id = 163,
                refinements = stateRefinements
            };

            List<RefinementDimensions> refinements = new List<RefinementDimensions>();
            refinements.Add(orDestinations);
            refinements.Add(orCity);
            refinements.Add(orState);
            refinements.Add(orNeighborhood);
            refinements.Add(orBedrooms);
            refinements.Add(orSleeps);
            refinements.Add(andAmenities);
            refinements.Add(orBuildingCategory);
            refinements.Add(orBuildingType);

            return refinements;
        }

        public List<Refinement> GetDestinationRefinements(List<PropertyHit> propertyHits)
        {
            List<Refinement> destinationRefinements = new List<Refinement>();

            GeographyQuery geographyQuery = new GeographyQuery();
            List<GeographyHit> gHits = new List<GeographyHit>();

            List<long> inventoryIDs = propertyHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();

            var subList = SplitIntoChunks<long>(inventoryIDs, 500);

            foreach (IEnumerable<long> ids in subList)
            {
                gHits.AddRange(geographyQuery.GetGeographyByInventoryIds(ids.ToList()).hits.hits.ToList());
            }

            List<Refinement> counts = gHits.GroupBy(x => x._source.Destination == null ? "" : x._source.Destination).Select(x => new Refinement()
            {
                name = x.Key,
                count = x.Distinct().Count(),
                urlValue = x.Key.Replace(" ", "--").ToLower()
            }).OrderBy(x => x.name).ToList();

            counts = (from x in counts
                      where x.name != ""
                      select x).ToList();

            destinationRefinements.AddRange(counts);

            return destinationRefinements;
        }

        public List<Refinement> GetCityRefinements(List<PropertyHit> propertyHits)
        {
            List<Refinement> cityRefinements = new List<Refinement>();

            GeographyQuery geographyQuery = new GeographyQuery();
            List<GeographyHit> gHits = new List<GeographyHit>();

            List<long> inventoryIDs = propertyHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();

            var subList = SplitIntoChunks<long>(inventoryIDs, 500);

            foreach (IEnumerable<long> ids in subList)
            {
                gHits.AddRange(geographyQuery.GetGeographyByInventoryIds(ids.ToList()).hits.hits.ToList());
            }

            List<Refinement> counts = gHits.GroupBy(x => x._source.CityName == null ? "" : x._source.CityName).Select(x => new Refinement()
            {
                name = x.Key,
                count = x.Distinct().Count(),
                urlValue = x.Key.Replace(" ", "--").ToLower()
            }).OrderBy(x => x.name).ToList();

            cityRefinements.AddRange(counts);

            return cityRefinements;
        }

        public List<Refinement> GetStateRefinements(List<PropertyHit> propertyHits)
        {
            List<Refinement> stateRefinements = new List<Refinement>();

            GeographyQuery geographyQuery = new GeographyQuery();
            List<GeographyHit> gHits = new List<GeographyHit>();

            List<long> inventoryIDs = propertyHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();
            var subList = SplitIntoChunks<long>(inventoryIDs, 500);

            foreach (IEnumerable<long> ids in subList)
            {
                gHits.AddRange(geographyQuery.GetGeographyByInventoryIds(ids.ToList()).hits.hits.ToList());
            }

            List<Refinement> counts = gHits.GroupBy(x => x._source.StateName == null ? "" : x._source.StateName).Select(x => new Refinement()
            {
                name = x.Key,
                count = x.Distinct().Count(),
                urlValue = x.Key.Replace(" ", "--").ToLower()
            }).OrderBy(x => x.name).ToList();

            counts = (from x in counts
                      where !x.name.Equals("")
                      select x).ToList();

            stateRefinements.AddRange(counts);

            return stateRefinements;
        }

        public List<Refinement> GetNeighborhoodRefinements(List<PropertyHit> propertyHits)
        {
            List<Refinement> neighborhoodRefinements = new List<Refinement>();

            NeighborhoodQuery neighborhoodQuery = new NeighborhoodQuery();
            List<NeighborhoodHit> neighborhoodHits = new List<NeighborhoodHit>();

            List<long> inventoryIDs = propertyHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();
            var subList = SplitIntoChunks<long>(inventoryIDs, 500);

            foreach (IEnumerable<long> ids in subList)
            {
                neighborhoodHits.AddRange(neighborhoodQuery.GetNeighborhoodByInventoryIDs(ids.ToList()).hits.hits.ToList());
            }

            neighborhoodRefinements.AddRange(neighborhoodHits.GroupBy(x => x._source.Neighborhood == null ? "" : x._source.Neighborhood).Select(x => new Refinement()
            {
                name = x.Key,
                count = x.Distinct().Count(),
                urlValue = x.Key.Replace(" ", "--").ToLower()
            }).OrderBy(x => x.name));

            return neighborhoodRefinements;
        }

        public List<Refinement> GetBedroomRefinements(List<PropertyHit> propertyHits)
        {
            List<Refinement> bedroomRefinements = new List<Refinement>();

            List<PropertyHit> oneBedHits = (from x in propertyHits
                                            where x._source.Bedroom == 1
                                            select x).ToList();

            List<PropertyHit> twoBedHits = (from x in propertyHits
                                            where x._source.Bedroom == 2
                                            select x).ToList();

            List<PropertyHit> threeBedHits = (from x in propertyHits
                                              where x._source.Bedroom == 3
                                              select x).ToList();

            List<PropertyHit> fourBedHits = (from x in propertyHits
                                             where x._source.Bedroom == 4
                                             select x).ToList();

            List<PropertyHit> fivePlusBedHits = (from x in propertyHits
                                                 where x._source.Bedroom >= 5
                                                 select x).ToList();

            Refinement oneBed = new Refinement()
            {
                name = "1 BR",
                count = oneBedHits.Count,
                urlValue = "1--BR"
            };
            Refinement twoBed = new Refinement()
            {
                name = "2 BR",
                count = twoBedHits.Count,
                urlValue = "2--BR"
            };
            Refinement threeBed = new Refinement()
            {
                name = "3 BR",
                count = threeBedHits.Count,
                urlValue = "3--BR"
            };
            Refinement fourBed = new Refinement()
            {
                name = "4 BR",
                count = fourBedHits.Count,
                urlValue = "4--BR"
            };
            Refinement fivePlusBed = new Refinement()
            {
                name = "5+ BR",
                count = fivePlusBedHits.Count,
                urlValue = "lots"
            };

            if (oneBed.count > 0)
                bedroomRefinements.Add(oneBed);
            if (twoBed.count > 0)
                bedroomRefinements.Add(twoBed);
            if (threeBed.count > 0)
                bedroomRefinements.Add(threeBed);
            if (fourBed.count > 0)
                bedroomRefinements.Add(fourBed);
            if (fivePlusBed.count > 0)
                bedroomRefinements.Add(fivePlusBed);

            return bedroomRefinements;
        }

        public List<Refinement> GetSleepsRefinements(List<PropertyHit> propertyHits)
        {
            List<Refinement> sleepsRefinement = new List<Refinement>();

            List<PropertyHit> oneToFourHits = (from x in propertyHits
                                               where x._source.Sleeps == 1 ||
                                               x._source.Sleeps == 2 ||
                                               x._source.Sleeps == 3 ||
                                               x._source.Sleeps == 4
                                               select x).ToList();

            List<PropertyHit> fiveToEightHits = (from x in propertyHits
                                                 where x._source.Sleeps == 5 ||
                                                 x._source.Sleeps == 6 ||
                                                 x._source.Sleeps == 7 ||
                                                 x._source.Sleeps == 8
                                                 select x).ToList();

            List<PropertyHit> nineToTwleveHits = (from x in propertyHits
                                                  where x._source.Sleeps == 9 ||
                                                  x._source.Sleeps == 10 ||
                                                  x._source.Sleeps == 11 ||
                                                  x._source.Sleeps == 12
                                                  select x).ToList();

            List<PropertyHit> thirteenPlusHits = (from x in propertyHits
                                                  where x._source.Sleeps >= 13
                                                  select x).ToList();

            Refinement oneToFour = new Refinement()
            {
                name = "Sleeps 1-4",
                count = oneToFourHits.Count,
                urlValue = "1-4"
            };
            Refinement fiveToEight = new Refinement()
            {
                name = "Sleeps 5-8",
                count = fiveToEightHits.Count,
                urlValue = "5-8"
            };
            Refinement nineToTwelve = new Refinement()
            {
                name = "Sleeps 9-12",
                count = nineToTwleveHits.Count,
                urlValue = "9-12"
            };
            Refinement thirteenPlus = new Refinement()
            {
                name = "Sleeps 13+",
                count = thirteenPlusHits.Count,
                urlValue = "lots"
            };
            if (oneToFour.count > 0)
                sleepsRefinement.Add(oneToFour);
            if (fiveToEight.count > 0)
                sleepsRefinement.Add(fiveToEight);
            if (nineToTwelve.count > 0)
                sleepsRefinement.Add(nineToTwelve);
            if (thirteenPlus.count > 0)
                sleepsRefinement.Add(thirteenPlus);

            return sleepsRefinement;
        }

        public List<Refinement> GetAmenitiesRefinements(List<PropertyHit> propertyHits)
        {
            AmenityQuery amenityQuery = new AmenityQuery();
            List<AmenityHit> amenities = new List<AmenityHit>();
            List<Refinement> amenitiesRefinement = new List<Refinement>();

            //List<string> names = new List<string>() { "Air Conditioning", "Pool and Spa" };
            EndecaAmenitiesViewRepository _endecaAmenitiesViewRepository = new EndecaAmenitiesViewRepository();
            List<string> names = _endecaAmenitiesViewRepository.GetSplurgeAmenities();

            List<long> inventoryIDs = propertyHits.Select(x => x._source.InventoryID).ToList();

            var subList = SplitIntoChunks<long>(inventoryIDs, 500);

            foreach (IEnumerable<long> ids in subList)
            {
                amenities.AddRange(amenityQuery.GetAmenitiesByInventoryIDs(ids.ToList()).hits.hits.ToList());
            }

            amenities = (from x in amenities
                         join y in names on
                         x._source.AttribName.ToLower() equals y.ToLower()
                         select x).ToList();

            List<Refinement> counts = amenities.GroupBy(x => x._source.AttribName).Select(x => new Refinement()
            {
                name = x.Key.Trim(),
                count = x.Distinct().Count(),
                urlValue = x.Key.Replace(" ", "--"),
            }).OrderBy(x => x.name).ToList();

            counts = (from x in counts
                      where !x.name.Contains("\"") && !x.name.Equals(string.Empty) && !x.name.Contains("`") && !x.name.Contains("'")
                      select x).ToList();

            amenitiesRefinement.AddRange(counts);

            return amenitiesRefinement;
        }

        public List<Refinement> GetBuildingCategoryRefinements(List<PropertyHit> propertyHits)
        {
            RentalUnitContentQuery rentalUnitContentQuery = new RentalUnitContentQuery();
            List<RentalUnitContentHit> rentalHits = new List<RentalUnitContentHit>();
            List<Refinement> buildingCategoryRefinement = new List<Refinement>();

            List<long> inventoryIDs = propertyHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();

            var subList = SplitIntoChunks<long>(inventoryIDs, 500);

            foreach (IEnumerable<long> ids in subList)
            {
                rentalHits.AddRange(rentalUnitContentQuery.GetRentalUnitContentByInventoryIDs(ids.ToList()).hits.hits.ToList());
            }

            int hotelCount = (from x in rentalHits
                             where x._source.BuildingType != null &&
                             x._source.BuildingType.ToLower().Contains("hotel")
                             select x).Count();

            int homeCount = (from x in rentalHits
                              where x._source.BuildingType != null && 
                              x._source.BuildingType.ToLower().Contains("home")
                              select x).Count();

            int condoCount = (from x in rentalHits
                             where x._source.BuildingType != null && 
                             x._source.BuildingType.ToLower().Contains("condo")
                             select x).Count();

            Refinement hotel = new Refinement()
            {
                name = "Hotel",
                count = hotelCount,
                urlValue = "hotel"
            };

            Refinement home = new Refinement()
            {
                name = "Home",
                count = homeCount,
                urlValue = "home"
            };

            Refinement condo = new Refinement()
            {
                name = "Condo",
                count = condoCount,
                urlValue = "condo"
            };

            buildingCategoryRefinement.Add(hotel);
            buildingCategoryRefinement.Add(condo);
            buildingCategoryRefinement.Add(home);

            return buildingCategoryRefinement;
        }

        public List<Refinement> GetBuildingTypeRefinements(List<PropertyHit> propertyHits)
        {
            //List<Refinement> buildingTypeRefinement = new List<Refinement>();

            //List<Refinement> counts = propertyHits.GroupBy(x => x._source.BuildingType == null ? "" : x._source.BuildingType).Select(x => new Refinement()
            //{
            //    name = x.Key.Replace("CONDO_", "").Replace("HOME_", "").Replace("HOTEL_", ""),
            //    count = x.Distinct().Count(),
            //    urlValue = x.Key.Replace(" ", "--").ToLower()
            //}).OrderBy(x => x.name).ToList();

            //List<Refinement> remove = (from x in counts
            //                         where x.name.Equals("")
            //                         select x).ToList();

            //foreach(Refinement r in remove)
            //{
            //    counts.Remove(r);
            //}

            //buildingTypeRefinement.AddRange(counts);

            //return buildingTypeRefinement;
            return new List<Refinement>();
        }

        public List<PropertyHit> FilterByRefinements(SelectedDimension[] selectedDimensions, List<PropertyHit> propertyHits)
        {
            List<PropertyHit> refinedProperties = new List<PropertyHit>();

            List<PropertyHit> destinationProperties = new List<PropertyHit>();
            List<PropertyHit> cityProperties = new List<PropertyHit>();
            List<PropertyHit> stateProperties = new List<PropertyHit>();
            List<PropertyHit> neighborhoodProperties = new List<PropertyHit>();
            List<PropertyHit> sleepsProperties = new List<PropertyHit>();
            List<PropertyHit> bedroomsProperties = new List<PropertyHit>();
            List<PropertyHit> amenitiesProperties = new List<PropertyHit>();
            List<PropertyHit> buildingCategoriesProperties = new List<PropertyHit>();
            List<List<PropertyHit>> refinementPropertyList = new List<List<PropertyHit>>();

            List<SelectedDimension> destinationRefinements = new List<SelectedDimension>();
            List<SelectedDimension> cityRefinements = new List<SelectedDimension>();
            List<SelectedDimension> stateRefinements = new List<SelectedDimension>();
            List<SelectedDimension> neighborhoodRefinements = new List<SelectedDimension>();
            List<SelectedDimension> sleepsRefinements = new List<SelectedDimension>();
            List<SelectedDimension> bedroomsRefinements = new List<SelectedDimension>();
            List<SelectedDimension> amenitiesRefinements = new List<SelectedDimension>();
            List<SelectedDimension> buildingCategoriesRefinements = new List<SelectedDimension>();

            List<long> inventoryIDs = propertyHits.Select(x => x._source.InventoryID).ToList();

            //seperate each refinement by type
            foreach (SelectedDimension sd in selectedDimensions)
            {
                if (sd.searchUrl.ToLower().Contains("destination_or"))
                {
                    destinationRefinements.Add(sd);
                }
                if (sd.searchUrl.ToLower().Contains("city_or"))
                {
                    cityRefinements.Add(sd);
                }
                if (sd.searchUrl.ToLower().Contains("state_or"))
                {
                    stateRefinements.Add(sd);
                }
                if (sd.searchUrl.ToLower().Contains("neighborhood_or"))
                {
                    neighborhoodRefinements.Add(sd);
                }
                if (sd.searchUrl.ToLower().Contains("sleeps_or"))
                {
                    sleepsRefinements.Add(sd);
                }
                if (sd.searchUrl.ToLower().Contains("bedrooms_or"))
                {
                    bedroomsRefinements.Add(sd);
                }
                if (sd.searchUrl.ToLower().Contains("amenities_and"))
                {
                    amenitiesRefinements.Add(sd);
                }
                if (sd.searchUrl.ToLower().Contains("building_category_or"))
                {
                    buildingCategoriesRefinements.Add(sd);
                }
            }

            //load properties by each type
            foreach (SelectedDimension sd in destinationRefinements)
            {
                GeographyQuery geographyQuery = new GeographyQuery();
                PropertyQuery propertyQuery = new PropertyQuery();
                List<GeographyHit> gHits = new List<GeographyHit>();

                string q = sd.name.Replace("--", " ").Replace("*", " ");

                gHits = geographyQuery.GetGeographyByInventoryIds(inventoryIDs).hits.hits.ToList();
                gHits = (from x in gHits
                         where x._source.Destination != null
                         select x).ToList();
                gHits = (from x in gHits
                         where x._source.Destination.ToLower().Equals(q.ToLower())
                         select x).ToList();

                List<long> ids = gHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();
                List<PropertyHit> hits = propertyQuery.GetPropertyByInventoryIDs(ids).hits.hits.ToList();

                destinationProperties.AddRange(hits);
            }
            foreach (SelectedDimension sd in cityRefinements)
            {
                GeographyQuery geographyQuery = new GeographyQuery();
                PropertyQuery propertyQuery = new PropertyQuery();
                List<GeographyHit> gHits = new List<GeographyHit>();

                string q = sd.name.Replace("--", " ").Replace("*", " "); ;

                gHits = geographyQuery.GetGeographyByInventoryIds(inventoryIDs).hits.hits.ToList();
                gHits = (from x in gHits
                         where x._source.CityName.ToLower().Equals(q.ToLower())
                         select x).ToList();
                List<long> ids = gHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();
                List<PropertyHit> hits = propertyQuery.GetPropertyByInventoryIDs(ids).hits.hits.ToList();

                cityProperties.AddRange(hits);
            }
            foreach (SelectedDimension sd in stateRefinements)
            {
                GeographyQuery geographyQuery = new GeographyQuery();
                PropertyQuery propertyQuery = new PropertyQuery();
                List<GeographyHit> gHits = new List<GeographyHit>();

                string q = sd.name.Replace("--", " ").Replace("*", " "); ;

                gHits = geographyQuery.GetGeographyByInventoryIds(inventoryIDs).hits.hits.ToList();
                gHits = (from x in gHits
                         where x._source.StateName.ToLower().Equals(q.ToLower())
                         select x).ToList();
                List<long> ids = gHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();
                List<PropertyHit> hits = propertyQuery.GetPropertyByInventoryIDs(inventoryIDs).hits.hits.ToList();

                stateProperties.AddRange(hits);
            }
            foreach (SelectedDimension sd in neighborhoodRefinements)
            {
                NeighborhoodQuery neighborhoodQuery = new NeighborhoodQuery();
                PropertyQuery propertyQuery = new PropertyQuery();
                List<NeighborhoodHit> neighborhoodHits = new List<NeighborhoodHit>();

                string q = sd.name.Replace("--", " ").Replace("*", " "); ;

                neighborhoodHits = neighborhoodQuery.GetNeighborhoodByInventoryIDs(inventoryIDs).hits.hits.ToList();
                neighborhoodHits = (from x in neighborhoodHits
                                    where x._source.Neighborhood.ToLower().Equals(q.ToLower())
                                    select x).ToList();
                List<long> ids = neighborhoodHits.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();
                List<PropertyHit> hits = propertyQuery.GetPropertyByInventoryIDs(ids).hits.hits.ToList();

                neighborhoodProperties.AddRange(hits);
            }
            foreach (SelectedDimension sd in sleepsRefinements)
            {
                string q = sd.name.Replace("--", " ").Replace("*", " "); ;

                if (q.Contains("lots"))
                {
                    List<PropertyHit> hits = (from x in propertyHits
                                              where x._source.Sleeps >= 13
                                              select x).ToList();

                    sleepsProperties.AddRange(hits);
                }
                else
                {
                    string[] sleepsHighLow = q.ToLower().Replace("sleeps", "").Split('-');
                    int low = int.Parse(sleepsHighLow[0]);
                    int high = int.Parse(sleepsHighLow[1]);

                    for (int i = low; i <= high; i++)
                    {
                        List<PropertyHit> hits = (from x in propertyHits
                                                  where x._source.Sleeps == i
                                                  select x).ToList();

                        sleepsProperties.AddRange(hits);
                    }
                }
            }
            foreach (SelectedDimension sd in bedroomsRefinements)
            {
                string q = sd.name.ToLower().Replace("*", " ").Replace("--br", "");

                if (q.Contains("lots"))
                {
                    List<PropertyHit> hits = (from x in propertyHits
                                              where x._source.Bedroom >= 5
                                              select x).ToList();

                    bedroomsProperties.AddRange(hits);
                }
                else
                {
                    int beds = int.Parse(q);

                    List<PropertyHit> hits = (from x in propertyHits
                                              where x._source.Bedroom == beds
                                              select x).ToList();

                    bedroomsProperties.AddRange(hits);
                }
            }
            foreach (SelectedDimension sd in amenitiesRefinements)
            {
                AmenityQuery amenityQuery = new AmenityQuery();
                PropertyQuery propertyQuery = new PropertyQuery();
                List<AmenityHit> amenities = new List<AmenityHit>();

                var subList = SplitIntoChunks<long>(inventoryIDs, 500);

                foreach (IEnumerable<long> idss in subList)
                {
                    amenities.AddRange(amenityQuery.GetAmenitiesByInventoryIDs(idss.ToList()).hits.hits.ToList());
                }

                string q = sd.name.Replace("--", " ").Replace("*", " ").Trim();

                amenities = (from x in amenities
                             where x._source.AttribName.Trim().ToLower().Equals(q.ToLower())
                             select x).ToList();

                List<long> ids = amenities.GroupBy(x => x._source.InventoryID).Select(x => x.Key).ToList();
                List<PropertyHit> hits = propertyQuery.GetPropertyByInventoryIDs(ids).hits.hits.ToList();

                if (amenitiesProperties.Count > 0)
                    amenitiesProperties = (from x in amenitiesProperties
                                           join y in hits
                                           on x._source.InventoryID equals y._source.InventoryID
                                           select x).ToList();
                else
                    amenitiesProperties = hits;
            }
            foreach (SelectedDimension sd in buildingCategoriesRefinements)
            {
                if (sd.name.ToLower().Contains("home"))
                {
                    buildingCategoriesProperties.AddRange((from x in propertyHits
                                               where x._source.BuildingType.ToLower().Contains("home")
                                               select x).ToList());
                }
                if (sd.name.ToLower().Contains("hotel"))
                {
                    buildingCategoriesProperties.AddRange((from x in propertyHits
                                               where x._source.BuildingType.ToLower().Contains("hotel")
                                               select x).ToList());
                }
                if (sd.name.ToLower().Contains("condo"))
                {
                    buildingCategoriesProperties.AddRange((from x in propertyHits
                                               where x._source.BuildingType.ToLower().Contains("condo")
                                               select x).ToList());
                }
            }

            if (destinationProperties.Count > 0)
                refinementPropertyList.Add(destinationProperties);
            if (cityProperties.Count > 0)
                refinementPropertyList.Add(cityProperties);
            if (stateProperties.Count > 0)
                refinementPropertyList.Add(stateProperties);
            if (neighborhoodProperties.Count > 0)
                refinementPropertyList.Add(neighborhoodProperties);
            if (sleepsProperties.Count > 0)
                refinementPropertyList.Add(sleepsProperties);
            if (bedroomsProperties.Count > 0)
                refinementPropertyList.Add(bedroomsProperties);
            if (buildingCategoriesProperties.Count > 0)
                refinementPropertyList.Add(buildingCategoriesProperties);
            if (amenitiesProperties.Count > 0)
                refinementPropertyList.Add(amenitiesProperties);

            foreach (List<PropertyHit> hits in refinementPropertyList)
            {
                //get a list to start with
                if (refinedProperties.Count == 0)
                    refinedProperties = hits;
                else
                {
                    refinedProperties = (from x in refinedProperties
                                         join y in hits
                                         on x._source.InventoryID equals y._source.InventoryID
                                         select x).ToList();
                }
            }

            return refinedProperties.GroupBy(x => x._source.InventoryID).Select(x => x.First()).ToList();
        }

        /// <summary>
        /// Fill propertyInfo objects, from given PropertyHits
        /// </summary>
        /// <param name="propertyHits"></param>
        /// <param name="infos"></param>
        public void FillPropertyInfos(List<PropertyHit> propertyHits, List<PropertyInfo> infos)
        {
            Task[] tasks = new Task[propertyHits.Count];

            foreach (PropertyHit propHit in propertyHits)
            {
                PropertySource pv = propHit._source;

                RentalUnitContentQuery rentalUnitContentQuery = new RentalUnitContentQuery();
                ElasticRentalUnitContentSearchResults rus = rentalUnitContentQuery.GetRentalUnitContentByInventoryID(pv.InventoryID);
                RentalUnitContentSource ru = rus.hits.hits[0]._source;

                GeographyQuery geographyQuery = new GeographyQuery();
                ElasticGeographySearchResult gvs = geographyQuery.GetGeographyByInventoryId(pv.InventoryID);
                GeographySource gv = gvs.hits.hits[0]._source;

                WeeklyRateQuery weeklyRateQuery = new WeeklyRateQuery();
                ElasticWeeklyRateSearchResult weeklyResult = weeklyRateQuery.GetWeeklyRateByInventoryID(pv.InventoryID);

                //WeeklyRateSource weeklySource = weeklyResult.hits.hits.Count() >= 1 ? weeklyResult.hits.hits[0]._source : new WeeklyRateSource();
                WeeklyRateSource weeklySource = weeklyResult.hits.hits[0]._source;

                PropertyInfo pi = new PropertyInfo();
                pi.spec = pv.InventoryID.ToString();
                pi.RentalUnitId = (int)ru.RentalUnitId;
                pi.InventoryId = pv.InventoryID;
                pi.PromoText = "";
                pi.Description = pv.InventoryDescription;

                pi.ImageCount = ru.ImageCount;
                pi.Sleeps = pv.Sleeps;
                pi.VirtualTourUrl = pv.VirtualTourURL;
                pi.ExpertReview = pv.ExpertReview;
                pi.EmailBooking = pv.EmailBooking.ToString();
                pi.PropertyDisclaimer = pv.PropertyDisclaimer;
                pi.Policies = pv.Policies;

                pi.IsLuxury = pv.IsLuxury;
                pi.Address1 = ru.Address1;
                pi.Address2 = ru.Address2;
                pi.City = gv.CityName;
                pi.State = gv.StateName;
                pi.StateCode = gv.StateCode;
                pi.PostalCode = ru.PostalCode;
                pi.Country = gv.Country;
                pi.CountryCode = gv.CountryCode;
                #region latLongParse
                string[] latLong = pv.GeoPosition == null ? new string[2] : pv.GeoPosition.Split(',');
                string lat = latLong[0];
                string lng = latLong[1];
                #endregion latLongParse
                pi.Latitude = lat != null ? float.Parse(lat) : (float?)null;
                pi.Longitude = lng != null ? float.Parse(lng) : (float?)null;
                pi.Island = gv.IslandName;
                pi.Status = pv.Status;
                pi.FromRate = (decimal)weeklySource.FromRate;
                pi.SpecialFromRate = (decimal)weeklySource.SpecialFromRate;
                pi.FromRateEndDate = weeklySource.FromRateEndDate == null ? (DateTime?)null : DateTime.Parse(weeklySource.FromRateEndDate);
                pi.FromRateStartDate = weeklySource.FromRateStartDate == null ? (DateTime?)null : DateTime.Parse(weeklySource.FromRateStartDate);
                pi.CurrencyType = pv.CurrencyType;
                pi.SpecialDescription = "";
                pi.SpecialStartDate = null;////
                pi.SpecialEndDate = null;////
                pi.SpecialBookByDate = null;////////
                pi.SpecialExclusions = null;////
                pi.SpecialTermsAndConditions = null;////

                //VacationRoost.Domain.GatewayProviderType x = new VacationRoost.Domain.GatewayProviderType;
                //GatewayProviderType.Lookup(pv.GatewayProviderTypeCode);
                pi.GatewayProviderType = null;// GatewayProviderType.Lookup(pv.GatewayProviderTypeCode);// null;// VacationRoost.Domain.GatewayProviderType;
                pi.GatewayPropertyTypeCode = pv.GatewayProviderTypeCode;
                string propRating = pv.PropertyRating == null ? "" : pv.PropertyRating.ToString();
                pi.PropertyRating = (propRating == string.Empty || propRating == null) ? null : (float?)float.Parse(propRating);

                infos.Add(pi);
            }
        }

        /// <summary>
        /// Get rates, to put into property hit objects, for sorting purposes
        /// </summary>
        /// <param name="hits"></param>
        public void FillInRatesAndMerchOrder(List<PropertyHit> hits)
        {
            NightlyAverageQuery nightlyAverageQuery = new NightlyAverageQuery();
            RentalUnitContentQuery rentalUnitContentQuery = new RentalUnitContentQuery();

            List<long> inventoryIDs = hits.Select(x => x._source.InventoryID).ToList();

            var subList = SplitIntoChunks<long>(inventoryIDs, 500);

            foreach(IEnumerable<long> ids in subList)
            {
                ElasticNightlyAverageSearchResults rateResults = nightlyAverageQuery.GetNightlyAverageRatesByInventoryIDs(ids.ToList());
                ElasticRentalUnitContentSearchResults rentalResults = rentalUnitContentQuery.GetRentalUnitContentByInventoryIDs(ids.ToList());

                foreach (NightlyAverageHit r in rateResults.hits.hits)
                {
                    hits.Select(x => x).Where(x => x._source.InventoryID == r._source.InventoryID).FirstOrDefault().rate = (double)r._source.YearAverageRate;
                }
                foreach (RentalUnitContentHit rh in rentalResults.hits.hits)
                {
                    hits.Select(x => x).Where(x => x._source.InventoryID == rh._source.InventoryId).FirstOrDefault().merchandiceOrder = rh._source.MerchandisingOrder;
                }
            }
        }

        public static List<List<T>> SplitIntoChunks<T>(List<T> list, int chunkSize)
        {
            if (chunkSize <= 0)
            {
                throw new ArgumentException("chunkSize must be greater than 0.");
            }

            List<List<T>> retVal = new List<List<T>>();
            int index = 0;
            while (index < list.Count)
            {
                int count = list.Count - index > chunkSize ? chunkSize : list.Count - index;
                retVal.Add(list.GetRange(index, count));

                index += chunkSize;
            }

            return retVal;
        }
    }
}