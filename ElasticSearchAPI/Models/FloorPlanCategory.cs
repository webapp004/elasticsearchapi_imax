﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class FloorPlanCategory : CodeValue<FloorPlanCategory>
    {
        public static class Codes
        {
            public const string HOTEL_ROOM = "HOTELROOM";
            public const string SUITE = "SUITE";
            public const string STUDIO = "STUDIO";
            public const string CONDO = "CONDO";
            public const string TOWNHOME = "TOWNHOME";
            public const string HOME = "HOME";
            public const string CAMPINGGROUND = "CAMPING_GROUND";
            public const string YACHT = "YACHT";
        }

        public readonly static FloorPlanCategory HotelRoom = new FloorPlanCategory(Codes.HOTEL_ROOM, "Hotel Room");
        public readonly static FloorPlanCategory Suite = new FloorPlanCategory(Codes.SUITE, "Suite");
        public readonly static FloorPlanCategory Studio = new FloorPlanCategory(Codes.STUDIO, "Studio");
        public readonly static FloorPlanCategory Condo = new FloorPlanCategory(Codes.CONDO, "Condo");
        public readonly static FloorPlanCategory Townhome = new FloorPlanCategory(Codes.TOWNHOME, "Townhome");
        public readonly static FloorPlanCategory Home = new FloorPlanCategory(Codes.HOME, "Home");
        public readonly static FloorPlanCategory CampingGround = new FloorPlanCategory(Codes.CAMPINGGROUND, "Camping Ground");
        public readonly static FloorPlanCategory Yacht = new FloorPlanCategory(Codes.YACHT, "Yacht");

        public FloorPlanCategory(string code, string description)
            : base(code, description)
        {
        }

    }
}